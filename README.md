# vue-02-monster-slayer
A simple 3-button game. Uses Vue.js, build as a part of the ["Vue JS 2 - The Complete Guide (incl. Vue Router & Vuex)"](https://www.udemy.com/vuejs-2-the-complete-guide/) online course.

## See it in action
**[Click.](https://htmlpreview.github.io/?https://github.com/Nick-Shmyrev/vue-02-monster-slayer/blob/master/index.html)**
