const vue = new Vue({
  el: "#app",
  data: {
    playerHP: 100,
    monsterHP: 100,
    gameIsRunning: false,
    combatLog: []
  },
  methods: {
    startGame: function () {
      "use strict";
      this.resetGame();
      this.gameIsRunning = true;
    },
    attack: function() {
      "use strict";
      const playerDmg = this.getRandomInt(0, 10);
      const monsterDmg = this.getRandomInt(0, 10);
      const d = new Date();
      const timestamp = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

      this.monsterHP -= playerDmg;
      this.playerHP -= monsterDmg;

      this.combatLog.unshift(`${timestamp} Monster attacked you for ${monsterDmg} Dmg`);
      this.combatLog.unshift(`${timestamp} You attacked monster for ${playerDmg} Dmg`);
    },
    specialAttack: function() {
      "use strict";
      const playerDmg = this.getRandomInt(0, 20);
      const monsterDmg = this.getRandomInt(0, 20);
      const d = new Date();
      const timestamp = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

      this.monsterHP -= playerDmg;
      this.playerHP -= monsterDmg;

      this.combatLog.unshift(`${timestamp} Monster used special attack to damage you for ${monsterDmg} Dmg`);
      this.combatLog.unshift(`${timestamp} You used special attack to damage monster for ${playerDmg} Dmg`);
    },
    heal: function() {
      "use strict";
      const playerHeal = this.getRandomInt(0, 15);
      const monsterHeal = this.getRandomInt(0, 15);
      const d = new Date();
      const timestamp = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

      if (this.monsterHP === 100) {
        this.combatLog.unshift(`${timestamp} Monster tried to heal itself, but it was already at MAX HP`);
      }
      else if (this.monsterHP < 100 && this.monsterHP + monsterHeal > 100) {
        this.combatLog.unshift(`${timestamp} Monster healed itself for ${100 - this.monsterHP} HP`);
        this.monsterHP = 100;
      } else {
        this.monsterHP += monsterHeal;
        this.combatLog.unshift(`${timestamp} Monster healed itself for ${monsterHeal} HP`);
      }

      if (this.playerHP === 100) {
        this.combatLog.unshift(`${timestamp} You tried to heal itself, but you were already at MAX HP`);
      }
      else if (this.playerHP < 100 && this.playerHP + playerHeal > 100) {
        this.combatLog.unshift(`${timestamp} You healed yourself for ${100 - this.playerHP} HP`);
        this.playerHP = 100;
      } else {
        this.playerHP += playerHeal;
        this.combatLog.unshift(`${timestamp} You healed yourself for ${playerHeal} HP`);
      }
    },
    giveUp: function() {
      "use strict";
      this.resetGame();
      this.gameIsRunning = false;
    },
    resetGame: function() {
      "use strict";
      this.playerHP = 100;
      this.monsterHP = 100;
      this.combatLog = [];
    },
    getRandomInt: function(min, max) {
      "use strict";
      return Math.floor(Math.random() * (max - min) + min);
    }
  },
  computed: {
    gameOver: function() {
      "use strict";
      if (this.playerHP <= 0) {return "lost";}
      else if (this.monsterHP <= 0) {return "won";}
    }
  },
  watch: {
    gameOver: function(result) {
      "use strict";
      switch (result) {
        case "lost":
          // setTimeout to allow HP bars to update before showing the game result
          setTimeout(() => {
            if (window.confirm('You lost! Play again?')) {
              this.startGame();
            } else {
              this.giveUp();
            }
          }, 10);
          break;
        case "won":
          // setTimeout to allow HP bars to update before showing the game result
          setTimeout(() => {
            if (window.confirm('You won! Play again?')) {
              this.startGame();
            } else {
              this.giveUp();
            }
          }, 10);
          break;
      }
    }
  }
});